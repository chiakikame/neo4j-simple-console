package io.thousandturtles

import org.neo4j.graphdb.GraphDatabaseService
import org.neo4j.graphdb.Result
import org.neo4j.graphdb.factory.GraphDatabaseFactory
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*

/*
 * Created by chiaki on 01/06/16.
 */
object Main {
    //
    // This variable is the default location of target database.
    // You can change database location here, or override it by
    // passing database location as command-line argument
    val defaultDatabaseLocation = """/tmp/test-db"""
    val logger = LoggerFactory.getLogger(Main.javaClass)

    @JvmStatic
    fun main(args: Array<String>) {
        val databaseLocation = if (args.size >= 1) {
            args[0]
        } else {
            defaultDatabaseLocation
        }

        logger.debug("Database path is $databaseLocation")

        val db = GraphDatabaseFactory().newEmbeddedDatabase(File(databaseLocation))
        Runtime.getRuntime().addShutdownHook(Thread({
            logger.debug("Shutting down db")
            db.shutdown()
        }))

        var queryString = ""
        val scanner = Scanner(System.`in`)
        println("Please add ';' at the end of the query.\nUse 'exit' to leave.")
        while(true) {
            if (queryString.isEmpty()) print("> ") else print("+ ")

            if (!scanner.hasNextLine()) break

            val line = scanner.nextLine()
            if (queryString.isEmpty()) {
                queryString = line
            } else {
                queryString += "\n$line"
            }

            if (queryString.toUpperCase() == "EXIT;" || queryString.toUpperCase() == "EXIT") {
                break
            } else if (queryString.endsWith(';')) {
                val qResult = db.runQuery(queryString)
                when  {
                    qResult.isLeft() -> {
                        println(qResult.left?.message)
                    }
                    qResult.isRight() -> {
                        val result = qResult.right ?: throw Exception("Logic error in program")
                        println(result)
                    }
                }
                queryString = ""
            }
        } // Ending line-reading loop
    }

    fun GraphDatabaseService.runQuery(query: String): Either<Exception, String> {
        val tx = beginTx()
        val excp: Exception
        try {
            val r = execute(query)
            tx.success()
            return Either.right(r.resultAsString())
        } catch(e: Exception) {
            tx.failure()
            excp = e
        } finally {
            tx.close()
        }
        return Either.left(excp)
    }
}