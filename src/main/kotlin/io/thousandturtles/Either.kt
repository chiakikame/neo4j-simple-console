package io.thousandturtles

/*
 * Created by chiaki on 01/06/16.
 */
class Either<L, R> {
    val left: L?
    val right: R?

    private constructor(): this(null, null)
    private constructor(left: L?, right: R?) {
        this.left = left
        this.right = right
    }

    fun isLeft(): Boolean = left != null
    fun isRight(): Boolean = right != null

    companion object {
        fun<L, R> left(l: L): Either<L, R> {
            return Either(l, null)
        }

        fun<L, R> right(r: R): Either<L, R> {
            return Either(null, r)
        }
    }
}